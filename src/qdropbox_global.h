#ifndef QDROPBOX_GLOBAL
#define QDROPBOX_GLOBAL

#include <QtCore/qglobal.h>
#include<QDebug>

#ifdef DROPBOXSTORAGE_LIBRARY
#  define DROPBOXSTORAGE_EXPORT Q_DECL_EXPORT
#else
#  define DROPBOXSTORAGE_EXPORT Q_DECL_IMPORT
#endif

#endif // QDROPBOX_GLOBAL

