#ifndef QDROPBOXDOWNLOADOBJECT
#define QDROPBOXDOWNLOADOBJECT

#include <QObject>
#include <QNetworkReply>
#include "qdropbox_global.h"
#include "qdropboxtypes.h"

class QTimer;
class QNetworkAccessManager;
class DROPBOXSTORAGE_EXPORT QDropboxDownload : public QObject
{
    Q_OBJECT

public:

    QDropboxDownload(QNetworkAccessManager *manager, const QNetworkRequest &request, QObject *parent = 0);
    ~QDropboxDownload();

    /*!
     * \brief readAll: reads all data available on the buffer. (Attention! If the function is called after finished() it will return empty QByteArray.) //TODO what happens if the function is called after finish(in order to read the rest of the bytes)
     * \return : QByteArray
     */
    QByteArray readAll() const;

//    /*!
//     * \brief state: returns the state of the download (see DownloadState enum)
//     * \param err: referenced parameter which keeps the error status which can be used by the caller of this method
//     * \return DownloadState
//     */
//    DownloadState state(ErrorType& err) const; //TODO this should be removed. ?

    /*!
     * \brief uid: returns the unique identifier which was generated in contructor. Used to uniquely determine each download object
     * \return QString of type UUID
     */
    QString uid() const;

    /*!
     * \brief start: starts the download
     */
    void start();

signals:
    /*!
     * \brief readyRead: signals if some incoming data were put into the buffer and are ready to read.
     * This signal may emit before any slot connects to it, so for these situations it is recommended to check for available
     * bytes with the "isBytesAvailable" method.
     */
    void readyRead();
    void downloadProgress(qint64 done, qint64 total);
    /*!
     * \brief finished: signals every time when the download finishes either with success nor with failure.
     */
    void finished(const ErrorType &replyCode, const QJsonDocument &jdoc);

private slots:
    /*!
     * \brief onAbort: slot called when timeout timer triggers. if there are no bytes available to read then the download is aborted due to timeout.
     * The timeout timer restarts every time readRead signal occurs. The role of the timer is to identify suspended downloads
     * (eg. client internet connection drops).
     */
    void onAbort();
    void onReadyRead();
    void onFinished();

private:
    QNetworkReply *m_reply;
    QNetworkAccessManager *m_manager;
    QNetworkRequest m_request;
    QTimer *m_timeout;
    QString m_uid;
//    DownloadState m_state;

    /// processes NetworkError errors and converts them to 'local' ErrorType errors (see ErrorType enum)
    ErrorType toErrorType(QNetworkReply::NetworkError e) const;
};

#endif // QDROPBOXDOWNLOADOBJECT

