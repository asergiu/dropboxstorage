#include "qdropboxdownload.h"

#include <qnetworkreply.h>
#include <QNetworkAccessManager>
#include <QFile>
#include <QTimer>
#include <QUuid>
#include <QJsonDocument>

QDropboxDownload::QDropboxDownload(QNetworkAccessManager *manager, const QNetworkRequest &request, QObject *parent)
    : QObject(parent)
{
    m_manager = manager;
    m_request = request;

    m_uid = QUuid::createUuid().toString();
}

void QDropboxDownload::start()
{
//    m_state = DownloadState::STARTED;

    m_timeout = new QTimer();
    m_timeout->setInterval(10000);
    connect(m_timeout, SIGNAL(timeout()), this, SLOT(onAbort()));
    m_timeout->start();

    m_reply = m_manager->post(m_request, QByteArray());

    connect(m_reply, SIGNAL(readyRead()), this, SIGNAL(readyRead()));
    connect(m_reply, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(m_reply, SIGNAL(downloadProgress(qint64,qint64)), this, SIGNAL(downloadProgress(qint64,qint64)));
    connect(m_reply, SIGNAL(finished()), this, SLOT(onFinished()));
}

QDropboxDownload::~QDropboxDownload()
{
}

QByteArray QDropboxDownload::readAll() const
{
    if(m_timeout) m_timeout->start();
    if(m_reply && !m_reply->isFinished())
        return m_reply->readAll();
    else
        return QByteArray();
}

//QDropboxDownload::DownloadState QDropboxDownload::state(ErrorType &err) const
//{
//    err = toErrorType(m_reply->error());
//    return m_state;
//}

QString QDropboxDownload::uid() const
{
    return m_uid;
}


void QDropboxDownload::onReadyRead()
{
//    m_state = QDropboxDownload::PROGRESS;
    m_timeout->start();
}

void QDropboxDownload::onAbort()
{
    if(m_reply->bytesAvailable() == 0) m_reply->abort();
}

void QDropboxDownload::onFinished()
{
//    m_state = QDropboxDownload::FINISHED;

    m_timeout->stop();
    delete m_timeout;
    m_timeout = NULL;

    m_reply->deleteLater();

    QJsonDocument jdoc;
    QNetworkReply::NetworkError error = m_reply->error();
    if(error == QNetworkReply::NoError){
        QByteArray errorJson = "{\"success\": \" download finished \" }";
        jdoc = QJsonDocument::fromJson(errorJson);
        emit finished(ErrorType::NoError, jdoc);
    }
    else{
        if(error >= 200 && error < 300) //2xx should receive json as a response from dropbox api
        {
            jdoc = QJsonDocument::fromJson(m_reply->readAll());
        }
        else
        {
            QByteArray errorJson = "{\"error_summary\": \"QNetworkReply error code is " + QByteArray::number(error) + "\" }";
            jdoc = QJsonDocument::fromJson(errorJson);
        }

        emit finished(ErrorType::OtherError, jdoc);
    }
}

ErrorType QDropboxDownload::toErrorType(QNetworkReply::NetworkError e) const
{
    if(e == QNetworkReply::NoError)
        return NoError;
    else
        return OtherError;
}

