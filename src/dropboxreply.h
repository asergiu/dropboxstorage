#ifndef DROPBOXREPLY_H
#define DROPBOXREPLY_H

#include <QObject>
#include <QTimer>
#include <QNetworkReply>

class DropboxReply : public QObject
{
    Q_OBJECT
public:
    explicit DropboxReply(QNetworkReply* reply, QObject *parent = 0);
    ~DropboxReply();

    static int getMaxTimeOutMs();
    static void setMaxTimeOutMs(const int &timeout);

    QNetworkReply *getReply() const;

    QString getUuid() const;
    void setUuid(const QString &uuid);

    int getRequestType() const;
    void setRequestType(int requestType);

signals:
    void timeoutExpired(QString uuid);

public slots:
    void onTimeout();

private:
    QTimer m_timer;
    QString m_uuid;
    QNetworkReply* m_reply;
    int m_requestType;

    static bool USE_TIMEOUT_TIMERS;
    static int MAX_TIMEOUT_MS;

};

#endif // DROPBOXREPLY_H
