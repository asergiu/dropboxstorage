#ifndef QDROPBOX
#define QDROPBOX

#include <QObject>
#include <QJsonDocument>
#include <QFile>
#include <QNetworkRequest>
#include <QPair>
#include <QQueue>
#include <QMap>
#include "qdropbox_global.h"
#include "qdropboxtypes.h"
//#include "qerrortype.h"

class QNetworkAccessManager;
class QNetworkReply;
class QDropboxDownload;
class DropboxReply;
class QDropboxUpload;
class DROPBOXSTORAGE_EXPORT QDropbox : public QObject
{
    Q_OBJECT

public:
    QDropbox(QString accessToken);
    QDropbox(QDropbox &otherDropbox);

    enum RequestType{
        Download = 1,
        Upload,
        ListFolder,
        GetLatestCursor,
        ListFolderContinue,
        ListFolderLongpoll,
        Move,
        Delete
    };

//    enum ReplyStatusCode{
//        Success = 1,
//        Error
//    };

    /*!
     * \brief The UploadMode enum
     * Add - Never overwrite the existing file. The autorename strategy is to append a number to the file name. For example, "document.txt" might become "document (2).txt".
     * Overwrite - Always overwrite the existing file. The autorename strategy is the same as it is for add.
     * Update - String Overwrite if the given "rev" matches the existing file's "rev". The autorename strategy is to append the string "conflicted copy" to the file name. For example, "document.txt" might become "document (conflicted copy).txt" or "document (Panda's conflicted copy).txt".
     */
    enum UploadMode{
        Add,
        Overwrite,
        Update
    };

    /*!
     * \brief uploadFile: Create a new file in Dropbox with the contents provided in the request. Do not use this to upload a file larger than 120 000 000 bytes, instead use uploadSessionStart. (see dropbox documentation)
     * \param dropboxFolderPath: Dropbox file path where it needs to be uploaded (do not include filename.extension! only the path to folder!)
     * \param clientPath: Path of the local file that needs to be uploaded
     * \param mode: Add, Overwrite, Update. Default is Add. (see UploadMode enum)
     * \param rev: Revision of the file (used in Update mode only)
     * \param autorename: renames the file if file conflict occurs. Default is set to false
     * \param mute: when set to true dropbox does not notify when file changes. Default set to false
     * \return QDropboxUpload object used to receive upload status or NULL if the request cannot be sent
     */
    QDropboxUpload* uploadFile(QString dropboxFolderPath, QString clientPath, UploadMode mode = QDropbox::Add, QString rev = "", bool autorename = false, bool mute = false);

    //TODO the following methods should be used to upload large files
    void uploadSessionStart();
    void uploadSessionAppendV2();
    void uploadSessionFinish();

    /*!
     * \brief downloadFile: Download a file from a user's Dropbox. Only MAX_ACTIVE_DOWNLOADS downloads can be at a a time.
     * \param dropboxFilePath: The path of the file to download.
     * \param rev: The revision of the file to download.(optional. if rev filed is specified then only the revision parameter will be taken into consideration)
     * \return QDropoxDownload object which can be used to get the file content and the download status
     */
    QDropboxDownload* downloadFile(QString dropboxFilePath, QString rev = "");

    /*!
     * \brief listFolder: Returns the contents of a dropbox folder.
     * \param dropboxFolderPath: The path to the folder you want to see the contents of.
     * \param recursive: If true, the list folder operation will be applied recursively to all subfolders and the response will contain contents of all subfolders. The default for this field is False.
     * \param includeMediaInfo: If true, FileMetadata.media_info is set for photo and video. The default for this field is False.
     * \param includeDelete: If true, the results will include entries for files and folders that used to exist but were deleted. The default for this field is False.
     * \param includeHasExplicitSharedMembers:  If true, the results will include a flag for each file indicating whether or not that file has any explicit members. The default for this field is False.
     */
    QString listFolder(QString dropboxFolderPath, bool recursive = false, bool includeMediaInfo = false, bool includeDeleted = false, bool includeHasExplicitSharedMembers = false);

    /*!
     * \brief getLatestCursor: A way to quickly get a cursor for the folder's state. Unlike list_folder, list_folder/get_latest_cursor doesn't return any entries. This endpoint is for app which only needs to know about new files and modifications and doesn't need to know about files that already exist in Dropbox.
     * \param dropboxFolderPath: The path to the folder you want to see the contents of.
     * \param recursive: If true, the list folder operation will be applied recursively to all subfolders and the response will contain contents of all subfolders. The default for this field is False.
     * \param includeMediaInfo: If true, FileMetadata.media_info is set for photo and video. The default for this field is False.
     * \param includeDeleted: If true, the results will include entries for files and folders that used to exist but were deleted. The default for this field is False.
     * \param includeHasExplicitSharedMembers:  If true, the results will include a flag for each file indicating whether or not that file has any explicit members. The default for this field is False.
     */
    QString getLatestCursor(QString dropboxFolderPath, bool recursive = false, bool includeMediaInfo = false, bool includeDeleted = false, bool includeHasExplicitSharedMembers = false);

    /*!
     * \brief listFolderContinue: Once a cursor has been retrieved from list_folder, use this to paginate through all files and retrieve updates to the folder.
     * \param cursor The cursor returned by your last call to list_folder or list_folder/continue.
     */
    QString listFolderContinue(QString cursor);

    /*!
     * \brief listFolderLongpoll: A longpoll endpoint to wait for changes on an account. In conjunction with list_folder/continue, this call gives you a low-latency way to monitor an account for file changes. The connection will block until there are changes available or a timeout occurs. This endpoint is useful mostly for client-side apps.
     * \param cursor: A cursor as returned by list_folder or list_folder/continue. Cursors retrieved by setting ListFolderArg.include_media_info to true are not supported.
     * \param timeout: A timeout in seconds. The request will block for at most this length of time, plus up to 90 seconds of random jitter added to avoid the thundering herd problem. Care should be taken when using this parameter, as some network infrastructure does not support long timeouts. The default for this field is 30. MAX is 480 seconds.
     */
    QString listFolderLongpoll(QString cursor, int timeout = 30);

    QString moveFile(QString sourcePath, QString destPath);

    /*!
     * \brief deleteFile: deletes a file from Dropbox
     * \param dropboxFolderPath: The path to the file to delete.
     */
    QString deleteFile(QString dropboxFilePath);

public slots:
    void response(QNetworkReply *reply);
    void replyTimeoutExpired(QString uuid);

signals:
    void responseReply(const QString &uuid, const QDropbox::RequestType &type, const ErrorType &replyCode, const QJsonDocument &jdoc);

private:
    QNetworkAccessManager *mManager;
    QByteArray mAuthorizationToken;
    QMap<QString, DropboxReply*> mRepliesMap;
    const long MAX_FILE_SIZE = 120000000;
    const QString LIST_FOLDER_URL = "https://api.dropboxapi.com/2/files/list_folder";
    const QString LIST_FOLDER_CONTINUE_URL = "https://api.dropboxapi.com/2/files/list_folder/continue";
    const QString LIST_FOLDER_LONGPOOL_URL = "https://notify.dropboxapi.com/2/files/list_folder/longpoll";
    const QString GET_CURSOR_URL = "https://api.dropboxapi.com/2/files/list_folder/get_latest_cursor";

    QDropboxDownload *sendDownloadRequest(QString dropboxApiArg = "");
    QNetworkReply *sendRequest(RequestType requestType, QUrl hostUrl, QByteArray byteArray, QString userAgent = "", QString contentType = "", QString authorizationToken = "", QString dropboxApiArg = "");
    inline QString addReplyToMap(QNetworkReply* r);
    inline QString getReplyUuid(QNetworkReply *r);
    inline bool removeReply(const QString &uuid);

};

#endif // QDROPBOX

