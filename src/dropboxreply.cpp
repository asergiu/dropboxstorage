#include "dropboxreply.h"
int DropboxReply::MAX_TIMEOUT_MS = 2000;
bool DropboxReply::USE_TIMEOUT_TIMERS = false;
DropboxReply::DropboxReply(QNetworkReply *reply, QObject *parent) : QObject(parent)
{
    m_reply = reply;

    if(USE_TIMEOUT_TIMERS){

        m_timer.setInterval(MAX_TIMEOUT_MS);
        m_timer.setSingleShot(true);
        m_timer.start();

        connect(&m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    }
}

DropboxReply::~DropboxReply()
{
    m_reply->deleteLater();
    m_timer.stop();
}

int DropboxReply::getMaxTimeOutMs()
{
    return MAX_TIMEOUT_MS;
}

void DropboxReply::setMaxTimeOutMs(const int &timeout){
    MAX_TIMEOUT_MS = timeout;
}

QNetworkReply *DropboxReply::getReply() const
{
    return m_reply;
}

QString DropboxReply::getUuid() const
{
    return m_uuid;
}

void DropboxReply::setUuid(const QString &uuid)
{
    m_uuid = uuid;
}

void DropboxReply::onTimeout()
{
    emit timeoutExpired(m_uuid);
}

int DropboxReply::getRequestType() const
{
    return m_requestType;
}

void DropboxReply::setRequestType(int requestType)
{
    m_requestType = requestType;
}
