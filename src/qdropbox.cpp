#include "qdropbox.h"
#include "qdropboxdownload.h"
#include "qdropboxupload.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFileInfo>
#include <QJsonObject>
#include <QQueue>
#include <QUuid>
#include <QtGlobal>
#include "dropboxreply.h"

QDropbox::QDropbox(QString accessToken)
{
    mAuthorizationToken = accessToken.toUtf8();
    mManager = new QNetworkAccessManager(this);
    connect(mManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(response(QNetworkReply*)));
}

QDropbox::QDropbox(QDropbox &otherDropbox)
{

}

//TODO must check the maximum threshold size for uploading a file(now is 120000000 bytes - heuristically determined)
QDropboxUpload* QDropbox::uploadFile(QString dropboxFolderPath, QString filePath, UploadMode mode, QString rev, bool autorename, bool mute)
{   
    dropboxFolderPath = dropboxFolderPath.replace("\\", "/");
    if(!dropboxFolderPath.endsWith("/")) dropboxFolderPath += "/";

    QFile *file = new QFile(filePath);
    if(!file->exists()){
        qDebug()<<"QDropbox - uploadFile : file " << filePath << " does not exist";
        return NULL;
    }
    if(file->size() > MAX_FILE_SIZE){
        qDebug()<<"QDropbox - uploadFile : file " << filePath << " exceeds 120 000 000 bytes. Use uploadSessionStart for large files.";
        return NULL;
    }

    QFileInfo fileInfo(filePath);

    QString upMode = mode == QDropbox::Add? "add" : mode == QDropbox::Overwrite? "overwrite": "update";
    QString updateRevStr = "";

    if(mode == QDropbox::Update)
    {
        if(rev.size() < 1){qDebug() << "QDropbox - uploadFile : update mode selected but revision is empty!"; return NULL;}
        updateRevStr = ",\"update\":\""+rev+"\"";
    }

    QString autorenameStr = autorename? "true" : "false";
    QString muteStr = mute? "true" : "false";

    QUrl hostUrl = QUrl("https://content.dropboxapi.com/2/files/upload");
    QByteArray userAgent = "api-explorer-client";
    QByteArray contentType = "application/octet-stream";
    QByteArray dropbox_api_arg = "{\"path\":\"" + dropboxFolderPath.toUtf8() + fileInfo.fileName().toUtf8() + "\",\"autorename\":"+autorenameStr.toUtf8()+",\"mode\": {\".tag\":\""+upMode.toUtf8()+"\""+updateRevStr.toUtf8()+"},\"mute\":"+muteStr.toUtf8()+"}";

    QNetworkRequest request(hostUrl);
    request.setAttribute(QNetworkRequest::User, QDropbox::Upload);
    request.setRawHeader("User-Agent", userAgent);
    request.setRawHeader("Content-Type", contentType);
    request.setRawHeader("Dropbox-API-Arg", dropbox_api_arg);
    request.setRawHeader("Authorization", mAuthorizationToken);
    request.setRawHeader("Content-Length", QString::number(file->size()).toUtf8());

    file->open(QIODevice::ReadOnly);
    QNetworkReply *r = mManager->post(request, file);

    return new QDropboxUpload(r, this);;
}

QDropboxDownload* QDropbox::downloadFile(QString dropboxFilePath, QString rev)
{
    if(rev.size() > 1) dropboxFilePath = "rev:"+rev;
    QByteArray dropbox_api_arg = "{\"path\":\""+dropboxFilePath.toUtf8()+"\"}";

    QNetworkRequest request;
    request.setUrl(QUrl("https://content.dropboxapi.com/2/files/download"));
    request.setAttribute(QNetworkRequest::User, QDropbox::Download);
    request.setRawHeader("User-Agent", QString("api-explorer-client").toUtf8());
    request.setRawHeader("Authorization", mAuthorizationToken);
    request.setRawHeader("Dropbox-API-Arg", dropbox_api_arg);

    return new QDropboxDownload(mManager, request);
}

QString QDropbox::listFolder(QString dropboxFolderPath, bool recursive, bool includeMediaInfo, bool includeDeleted, bool includeHasExplicitSharedMembers)
{
    QUrl url = QUrl(LIST_FOLDER_URL);
    QByteArray userAgent = "api-explorer-client";
    QByteArray contentType = "application/json";

    QString recursiveStr = recursive? "true" : "false";
    QString includeMediaInfoStr = includeMediaInfo? "true" : "false";
    QString includeDeletedStr = includeDeleted? "true" : "false";
    QString includeHasExplicitSharedMembersStr = includeHasExplicitSharedMembers? "true" : "false";
    QByteArray dataJson = "{'path':'"+dropboxFolderPath.toUtf8()+"', 'include_media_info':"+includeMediaInfoStr.toUtf8()+",'recursive':"+recursiveStr.toUtf8()+",'include_deleted':"+includeDeletedStr.toUtf8()+", 'include_has_explicit_shared_members':"+includeHasExplicitSharedMembersStr.toUtf8()+"}";
    dataJson.replace("\'","\"");

    QNetworkReply *reply = sendRequest(QDropbox::ListFolder, url, dataJson, userAgent, contentType, mAuthorizationToken);

    QString uuid =  addReplyToMap(reply);
//    qDebug()<<"--QDropbox::REQUEST listFolder: " << uuid;

    if(uuid.isEmpty())
        reply->deleteLater();
    return uuid;
}

QString QDropbox::getLatestCursor(QString dropboxFolderPath, bool recursive, bool includeMediaInfo, bool includeDeleted, bool includeHasExplicitSharedMembers)
{
    QUrl url = QUrl(GET_CURSOR_URL);
    QByteArray userAgent = "api-explorer-client";
    QByteArray contentType = "application/json";

    QString recursiveStr = recursive? "true" : "false";
    QString includeMediaInfoStr = includeMediaInfo? "true" : "false";
    QString includeDeletedStr = includeDeleted? "true" : "false";
    QString includeHasExplicitSharedMembersStr = includeHasExplicitSharedMembers? "true" : "false";
    QByteArray dataJson = "{'path':'"+dropboxFolderPath.toUtf8()+"', 'include_media_info':"+includeMediaInfoStr.toUtf8()+",'recursive':"+recursiveStr.toUtf8()+",'include_deleted':"+includeDeletedStr.toUtf8()+", 'include_has_explicit_shared_members':"+includeHasExplicitSharedMembersStr.toUtf8()+"}";
    dataJson.replace("\'","\"");

    QNetworkReply* reply = sendRequest(QDropbox::GetLatestCursor, url, dataJson, userAgent, contentType, mAuthorizationToken);

    QString uuid =  addReplyToMap(reply);
//    qDebug()<<"--QDropbox::REQUEST getLatestCursor: " << uuid;

    if(uuid.isEmpty())
        reply->deleteLater();
    return uuid;
}

QString QDropbox::listFolderContinue(QString cursor)
{
    QUrl url = QUrl(LIST_FOLDER_CONTINUE_URL);
    QByteArray userAgent = "api-explorer-client";
    QByteArray contentType = "application/json";

    QByteArray dataJson = "{'cursor':'"+cursor.toUtf8()+"'}";
    dataJson.replace("\'","\"");


    QNetworkReply* reply = sendRequest(QDropbox::ListFolderContinue, url, dataJson, userAgent, contentType, mAuthorizationToken);
    QString uuid =  addReplyToMap(reply);
//    qDebug()<<"--QDropbox::REQUEST listFolderContinue: " << uuid;

    if(uuid.isEmpty())
        reply->deleteLater();
    return uuid;
}

QString QDropbox::listFolderLongpoll(QString cursor, int timeout)
{
    QUrl url = QUrl(LIST_FOLDER_LONGPOOL_URL);
    QByteArray userAgent = "api-explorer-client";
    QByteArray contentType = "application/json";

    QByteArray dataJson = "{'cursor':'"+cursor.toUtf8()+"','timeout':" + QString::number(timeout).toUtf8() + "}";
    dataJson.replace("\'","\"");

    QNetworkReply* reply = sendRequest(QDropbox::ListFolderLongpoll, url, dataJson, userAgent, contentType);
    QString uuid =  addReplyToMap(reply);
//    qDebug()<<"--QDropbox::REQUEST listFolderLongpoll: " << uuid;

    if(uuid.isEmpty())
        reply->deleteLater();
    return uuid;
}

QString QDropbox::moveFile(QString sourcePath, QString destPath)
{
    sourcePath = sourcePath.replace("\\", "/");
    destPath = destPath.replace("\\", "/");

    QUrl hostUrl = QUrl("https://api.dropboxapi.com/2/files/move");
    QByteArray userAgent = "api-explorer-client";
    QByteArray contentType = "application/json";
    QByteArray dropbox_api_arg = "{\"from_path\":\"" + sourcePath.toUtf8() +
            "\", \"to_path\":\"" + destPath.toUtf8() +"\"}";

    QNetworkReply* reply = sendRequest(QDropbox::Move, hostUrl, dropbox_api_arg, userAgent, contentType,
                                       mAuthorizationToken, QByteArray());
    QString uuid =  addReplyToMap(reply);

    if(uuid.isEmpty())
        reply->deleteLater();

    return uuid;
}

QString QDropbox::deleteFile(QString dropboxFilePath)
{
    dropboxFilePath = dropboxFilePath.replace("\\", "/");

    QUrl hostUrl = QUrl("https://api.dropboxapi.com/2/files/delete");
    QByteArray userAgent = "api-explorer-client";
    QByteArray contentType = "application/json";
    QByteArray dropbox_api_arg = "{\"path\":\"" + dropboxFilePath.toUtf8() + "\"}";

    QNetworkReply* reply = sendRequest(QDropbox::Move, hostUrl, dropbox_api_arg, userAgent, contentType,
                                       mAuthorizationToken, QByteArray());

    QString uuid =  addReplyToMap(reply);

    if(uuid.isEmpty())
        reply->deleteLater();

    return uuid;
}

void QDropbox::response(QNetworkReply *reply)
{
    QNetworkRequest request = reply->request();
    int requestType = request.attribute(QNetworkRequest::User).toInt();

    int error = reply->error();
    QString uuid;
    if (error == QNetworkReply::NoError)
    {
        QJsonDocument jdoc;
        if(requestType == QDropbox::ListFolder ||
                requestType == QDropbox::GetLatestCursor ||
                requestType == QDropbox::ListFolderContinue ||
                requestType == QDropbox::ListFolderLongpoll ||
                requestType == QDropbox::Move)
        {
            QByteArray replyBytes = reply->readAll();
            jdoc = QJsonDocument::fromJson(replyBytes);
            uuid = getReplyUuid(reply);

            if(uuid.isEmpty())return;
            removeReply(uuid);
            emit responseReply(uuid, (RequestType)requestType, ErrorType::NoError, jdoc);
        }
    }
    else
    {
        if(requestType == QDropbox::ListFolder ||
                requestType == QDropbox::GetLatestCursor ||
                requestType == QDropbox::ListFolderContinue ||
                requestType == QDropbox::ListFolderLongpoll ||
                requestType == QDropbox::Move)
        {
            QByteArray replyBytes = reply->readAll();
            QJsonDocument jdoc;
            if(error >= 200 && error < 300) //2xx should receive json as a response
            {
                jdoc = QJsonDocument::fromJson(replyBytes);
            }
            else
            {
                replyBytes.replace('\"', '\'');
                QByteArray errorJson = "{\"error_summary\": \"" +replyBytes + "\" }";
                jdoc = QJsonDocument::fromJson(errorJson);
            }

            uuid = getReplyUuid(reply);
            if(uuid.isEmpty())return;
            removeReply(uuid);
            emit responseReply(uuid, (RequestType)requestType, ErrorType::OtherError, jdoc);
        }
    }

    //reply->deleteLater();
}

void QDropbox::replyTimeoutExpired(QString uuid)
{
    DropboxReply* replyMap = mRepliesMap[uuid];
    if(replyMap == 0){
        return;
    }

    QByteArray errorJson = "{\"error_summary\": \" QDropbox response timeout \" }";
    QJsonDocument jdoc = QJsonDocument::fromJson(errorJson);

    emit responseReply(uuid, (RequestType)replyMap->getRequestType(), ErrorType::OtherError, jdoc );

    removeReply(uuid);
}

QNetworkReply* QDropbox::sendRequest(RequestType requestType, QUrl hostUrl, QByteArray byteArray, QString userAgent, QString contentType, QString authorizationToken, QString dropboxApiArg)
{
    if(hostUrl.isEmpty()) {
        qDebug()<< Q_FUNC_INFO << " error: hostUrl cannot be empty!";
        return 0;
    }

    QNetworkRequest request(hostUrl);
    request.setUrl(hostUrl);
    request.setAttribute(QNetworkRequest::User, requestType);
    if(!userAgent.isEmpty())          request.setRawHeader("User-Agent", userAgent.toUtf8());
    if(!contentType.isEmpty())        request.setRawHeader("Content-Type", contentType.toUtf8());
    if(!authorizationToken.isEmpty()) request.setRawHeader("Authorization", authorizationToken.toUtf8());
    if(!dropboxApiArg.isEmpty())      request.setRawHeader("Dropbox-API-Arg", dropboxApiArg.toUtf8());

    return mManager->post(request, byteArray);
}

QString QDropbox::addReplyToMap(QNetworkReply *r)
{
    if(!r)
        return "";

    const int maxTrys = 5;
    int numTrys = 0;

    QString uuid = QUuid::createUuid().toString();

    while(mRepliesMap.contains(uuid) && numTrys < maxTrys){
        numTrys++;
        qDebug()<<Q_FUNC_INFO<<" Warning! "<<uuid<<" already in use! Retry with another one.";
        uuid = QUuid::createUuid().toString();
    }

    if(numTrys == maxTrys){
        return "";
    }

    DropboxReply* dbReply = new DropboxReply(r);
    dbReply->setUuid(uuid);
    connect(dbReply, SIGNAL(timeoutExpired(QString)), this, SLOT(replyTimeoutExpired(QString)));
    mRepliesMap.insert(uuid, dbReply);

    return uuid;
}

QString QDropbox::getReplyUuid(QNetworkReply *r)
{
    QMap<QString, DropboxReply*>::const_iterator it = mRepliesMap.constBegin();

    while (it != mRepliesMap.constEnd()) {
        if(it.value()->getReply() == r)
            return it.key();
        ++it;
    }

    return "";
}

bool QDropbox::removeReply(const QString &uuid)
{
    DropboxReply* reply = mRepliesMap[uuid];
    if(!reply){
        qDebug()<<Q_FUNC_INFO<<" Error! Reply "<<uuid<<" not found for deletion";
        return false;
    }

    mRepliesMap.remove(uuid);
    reply->deleteLater();

    return true;
}
