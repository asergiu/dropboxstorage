#include "qdropboxupload.h"

#include <QTimer>
#include <QUuid>
#include <QJsonDocument>

QDropboxUpload::QDropboxUpload(QNetworkReply *reply, QObject *parent)
    : QObject(parent),
      m_state(QDropboxUpload::STARTED)
{
    connect(reply, SIGNAL(uploadProgress(qint64,qint64)), this, SLOT(onProgress(qint64,qint64)));
    connect(reply, SIGNAL(finished()), this, SLOT(onFinished()));

    m_reply = reply;

    m_timeout = new QTimer();
    m_timeout->setInterval(10000);
    connect(m_timeout, SIGNAL(timeout()), this, SLOT(onAbort()));
    m_timeout->start();

    m_uid = QUuid::createUuid().toString();
}

QString QDropboxUpload::uid() const
{
    return m_uid;
}

QDropboxUpload::UploadState QDropboxUpload::state(ErrorType &err, QJsonDocument &jdoc) const
{
    QNetworkReply::NetworkError error = m_reply->error();
    if(error == QNetworkReply::NoError){
        QByteArray errorJson = "{\"success\": \" upload \" }";
        jdoc = QJsonDocument::fromJson(errorJson);
        err = ErrorType::NoError;
    }
    else{
        if(error >= 200 && error < 300) //2xx should receive json as a response from dropbox api
        {
            jdoc = QJsonDocument::fromJson(m_reply->readAll());
        }
        else
        {
            QByteArray errorJson = "{\"error_summary\": \"QNetworkReply error code is " + QByteArray::number(error) + "\" }";
            jdoc = QJsonDocument::fromJson(errorJson);
        }

        err = ErrorType::OtherError;
    }

    return m_state;
}

void QDropboxUpload::onAbort()
{
    m_reply->abort();
}

void QDropboxUpload::onProgress(qint64 done, qint64 total)
{
    m_state = QDropboxUpload::PROGRESS;

    m_timeout->start();

    emit uploadProgress(done, total);
}

void QDropboxUpload::onFinished()
{
    m_state = QDropboxUpload::FINISHED;

    m_timeout->stop();
    m_timeout->deleteLater();
    m_reply->deleteLater();

    QJsonDocument jdoc;
    QNetworkReply::NetworkError error = m_reply->error();
    if(error == QNetworkReply::NoError){
        QByteArray errorJson = "{\"success\": \" upload finished \" }";
        jdoc = QJsonDocument::fromJson(errorJson);
        emit finished(ErrorType::NoError, jdoc);
    }
    else{
        if(error >= 200 && error < 300) //2xx should receive json as a response from dropbox api
        {
            jdoc = QJsonDocument::fromJson(m_reply->readAll());
        }
        else
        {
            QByteArray errorJson = "{\"error_summary\": \"QNetworkReply error code is " + QByteArray::number(error) + "\" }";
            jdoc = QJsonDocument::fromJson(errorJson);
        }

        emit finished(ErrorType::OtherError, jdoc);
    }
}
