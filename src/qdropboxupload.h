#ifndef QDROPBOXUPLOAD_H
#define QDROPBOXUPLOAD_H

#include <QObject>
#include <QNetworkReply>
#include "qdropboxtypes.h"

class QTimer;
class QDropboxUpload : public QObject
{
    Q_OBJECT
public:
    enum UploadState{
        STARTED = 0,
        PROGRESS,
        FINISHED
    };

    QDropboxUpload(QNetworkReply *reply, QObject *parent = 0);

    /*!
     * \brief uid: returns the unique identifier which was generated in contructor. Used to uniquely determine each download object
     * \return QString of type UUID
     */
    QString uid() const;

    /*!
     * \brief state: returns the state of the upload (see UploadState enum)
     * \param err: referenced parameter which keeps the error status which can be used by the caller of this method
     * \para jdic: JSonDocument containing information about the error code
     * \return UploadState
     */
    UploadState state(ErrorType& err, QJsonDocument &jdoc) const;

signals:
    void uploadProgress(qint64 done, qint64 total);
    void finished(const ErrorType &replyCode, const QJsonDocument &jdoc);

private slots:
    /*!
     * \brief onAbort: slot called when timeout timer triggers. It aboarts the upload. Timer restarts everytime the uploadProgress changes
     *. The role of the timer is to identify suspended uploads (eg. client internet connection drops).
     */
    void onAbort();
    void onFinished();
    void onProgress(qint64 done, qint64 total);

private:
    QNetworkReply *m_reply;
    QTimer *m_timeout;
    QString m_uid;
    UploadState m_state;
};

#endif // QDROPBOXUPLOAD_H
