TEMPLATE = lib

CONFIG(release, debug|release): TARGET = 'dropbox_storage'$$QT_VERSION
CONFIG(debug, debug|release): TARGET = 'dropbox_storaged'$$QT_VERSION

DESTDIR = bin

QT += qml quick widgets

SOURCES += src/main.cpp \
    src/qdropbox.cpp \
    src/qdropboxdownload.cpp \
    src/qdropboxupload.cpp \
    src/dropboxreply.cpp

RESOURCES += qml.qrc

DEFINES += DROPBOXSTORAGE_LIBRARY

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    src/qdropbox.h \
    src/qdropbox_global.h \
    src/qdropboxdownload.h \
    src/qdropboxupload.h \
    src/dropboxreply.h \
    src/qdropboxtypes.h \
    src/qerrortype.h
